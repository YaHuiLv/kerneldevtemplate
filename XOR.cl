
typedef struct MbSamePixel {
    int same_y_pixel_num ;
    int same_u_pixel_num ;
    int same_v_pixel_num ;
    int dummy;
}Mb_Same_Pixel;
__kernel void xor_MB_Pixel( __global int stride,
              __global unsigned char *cur_data_component1,  //当前Frame或Tile的分量1指针
               __global unsigned char *cur_data_component2,  //当前Frame或Tile的分量2指针
               __global unsigned char *cur_data_component3,  //当前Frame或Tile的分量3指针,
               __global unsigned char *ref_data_component1,  //参考Frame或Tile的分量1指针
               __global unsigned char *ref_data_component2,  //参考Frame或Tile的分量2指针
               __global unsigned char *ref_data_component3,  //参考Frame或Tile的分量3指针,
               __local short *xor_mb, __global struct Mb_Same_Pixel *same_info){

    int gid0 = get_global_id(0);
    int gid1 = get_global_id(1);


    unsigned char *p_mb_y_cur_org = cur_data_component1 + gid1 * MB_HEIGHT * stride + gid0 * MB_WIDTH;
    unsigned char *p_mb_u_cur_org = cur_data_component2 + gid1 * MB_HEIGHT * stride + gid0 * MB_WIDTH;
    unsigned char *p_mb_v_cur_org = cur_data_component3 + gid1 * MB_HEIGHT * stride + gid0 * MB_WIDTH;
    unsigned char *p_mb_y_ref_org = ref_data_component1 + gid1 * MB_HEIGHT * stride + gid0 * MB_WIDTH;
    unsigned char *p_mb_u_ref_org = ref_data_component2 + gid1 * MB_HEIGHT * stride + gid0 * MB_WIDTH;
    unsigned char *p_mb_v_ref_org = ref_data_component3 + gid1 * MB_HEIGHT * stride + gid0 * MB_WIDTH;


for (int j = 0; j < MB_HEIGHT; j++) {
        unsigned char *p_mb_y_cur = p_mb_y_cur_org + j * stride;
        unsigned char *p_mb_u_cur = p_mb_u_cur_org + j * stride;
        unsigned char *p_mb_v_cur = p_mb_v_cur_org + j * stride;
        unsigned char *p_mb_y_ref = p_mb_y_ref_org + j * stride;
        unsigned char *p_mb_u_ref = p_mb_u_ref_org + j * stride;
        unsigned char *p_mb_v_ref = p_mb_v_ref_org + j * stride;
        for (int i = 0; i < MB_WIDTH; i++) {
            unsigned char xor_y = (*p_mb_y_cur) ^(*p_mb_y_ref);
            if (0 == xor_y) {
                same_info->same_y_pixel_num++;
                *(xor_mb + j * MB_WIDTH + i) = 256;
            } else {
                *(xor_mb + j * MB_WIDTH + i) = *p_mb_y_cur;
            }
            unsigned char xor_u = (*p_mb_u_cur) ^(*p_mb_u_ref);
            if (0 == xor_u) {
                same_info->same_u_pixel_num++;
                *(xor_mb + MB_PIXEL_COUNT + j * MB_WIDTH + i) = 256;
            } else {
                *(xor_mb + MB_PIXEL_COUNT + j * MB_WIDTH + i) = *p_mb_u_cur;
            }
            unsigned char xor_v = (*p_mb_v_cur) ^(*p_mb_v_ref);
            if (0 == xor_v) {
                same_info->same_v_pixel_num++;
                *(xor_mb + MB_PIXEL_COUNT + MB_PIXEL_COUNT + j * MB_WIDTH + i) = 256;
            } else {
                *(xor_mb + MB_PIXEL_COUNT + MB_PIXEL_COUNT + j * MB_WIDTH + i) = *p_mb_v_cur;
            }
            p_mb_y_cur++;               // move the pointer which point to a pixel
            p_mb_u_cur++;
            p_mb_v_cur++;
            p_mb_y_ref++;
            p_mb_u_ref++;
            p_mb_v_ref++;
        }
    }
 }