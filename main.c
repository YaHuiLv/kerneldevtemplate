#include "stdio.h"
#include "time.h"
#include "opencl/simpleCL.h"

#define DEVICE 0

#define  MB_WIDTH 16
#define  MB_HEIGHT 16

#define EDGE 16
void example1();

void example2();

void test_xor_MB_OpenCL();


struct Picture{
    unsigned char *comp1;
    unsigned char *comp2;
    unsigned char *comp3;
    unsigned int height;
    unsigned int width;
    unsigned int stride;

};

struct MbSamePixel {
    int same_y_pixel_num ;
    int same_u_pixel_num ;
    int same_v_pixel_num ;
    int dummy;
};
void generateRandomPicture(const int width, const int height, const int stride, const int value_range,struct Picture *picture);
int main() {
   // example1();
  //  example2();
    test_xor_MB_OpenCL();
    return 0;

}

void test_xor_MB_OpenCL(){

    const int width = 1920;
    const int height = 1080;
    const int value_range = 2;
   const int stride = width + 2 * EDGE ;
    struct Picture *picture_cur = (struct Picture*)malloc(sizeof(struct Picture));
    struct Picture *picture_ref = (struct Picture*)malloc(sizeof(struct Picture));;

  generateRandomPicture(width,height,stride,value_range,picture_cur);
  generateRandomPicture(width,height,stride,value_range,picture_ref);

    short *xor_mb = (short*)malloc(sizeof(short)*MB_HEIGHT*MB_WIDTH);
    struct MbSamePixel *same_info = ( struct MbSamPixel*)calloc(1, sizeof(struct MbSamePixel));

    size_t global_size[2], local_size[2];
    int found;
    sclHard *hardware;
    sclSoft software;
    sclKernel kernel;

    // Get the hardware
    hardware = sclGetAllHardware(&found);
    // Get the software
    software = sclGetCLSoftware("XOR.cl", hardware[DEVICE]);
    kernel = sclGetCLKernel(software, "xor_MB_Pixel");
    // Set NDRange dimensions
    global_size[0] = stride/MB_WIDTH;
    global_size[1] = (height+2*EDGE)/(MB_HEIGHT);
    local_size[0] = 1;
    local_size[1] = 1;

    cl_event event = sclManageArgsLaunchKernel(hardware[DEVICE], kernel, global_size, local_size,
                                               " %a %a %a %a %a %a %a %a %a",
                                               sizeof(int), stride,
                                               sizeof(unsigned char*), picture_cur->comp1,
                                               sizeof(unsigned char*),picture_cur->comp2,
                                               sizeof(unsigned char*),picture_cur->comp3,
                                               sizeof(unsigned char*), picture_ref->comp1,
                                               sizeof(unsigned char*),picture_ref->comp2,
                                               sizeof(unsigned char*),picture_ref->comp3,
                                               sizeof(short*),xor_mb,
                                                sizeof(struct MbSamePixel*),same_info);
    cl_ulong time = sclGetEventTime(*hardware, event);

    printf("%d\n,%d\n,%d\n",same_info->same_y_pixel_num,same_info->same_u_pixel_num,same_info->same_v_pixel_num);
}

void example1() {
    char buf[] = "Hello, World!";
    size_t global_size[2], local_size[2];
    int found, worksize;
    sclHard *hardware;
    sclSoft software;
    sclKernel kernel;

    // Target buffer just so we show we got the data from OpenCL
    worksize = strlen(buf);
    char buf2[worksize];
    buf2[0] = '?';
    buf2[worksize] = 0;

    // Get the hardware
    hardware = sclGetAllHardware(&found);
    // Get the software
    software = sclGetCLSoftware("example.cl", hardware[DEVICE]);
    kernel = sclGetCLKernel(software, "example");
    // Set NDRange dimensions
    global_size[0] = strlen(buf);
    global_size[1] = 1;
    local_size[0] = global_size[0];
    local_size[1] = 1;

    cl_event event = sclManageArgsLaunchKernel(hardware[DEVICE], kernel, global_size, local_size,
                                               " %r %w ",
                                               worksize, buf, worksize, buf2);
    cl_ulong time = sclGetEventTime(*hardware, event);
    // Finally, output out happy message.
    printf("\n");
    puts(buf2);
}

void example2() {
    float *vector;
    float value;
    int i, found;
    /* SimpleOpenCL types declaration */
    sclHard *hardware;
    sclSoft software;
    sclKernel kernel;

    /* NDRange 2D size initialization*/
    size_t global_size[2];
    size_t local_size[2];
    size_t dataLength = 134217728;
    size_t dataSize = sizeof(float) * dataLength;

    global_size[0] = dataLength;
    global_size[1] = 1;
    local_size[0] = 1;
    local_size[1] = 1;
    /*local_size[0]=1 might be necessary for CPU devices on apple machines*/


    /* Data generation */
    vector = (float *) malloc(dataSize);
    value = 3;
    for (i = 0; i < (int) dataLength; i++) {
        vector[i] = (float) i;
    }

    /* Hardware and Software initialization ##### HERE STARTS THE SimpleOpenCL CODE ####*/
    found = 0;
    hardware = sclGetAllHardware(&found);
    software = sclGetCLSoftware("example2.cl", hardware[DEVICE]);
    kernel = sclGetCLKernel(software, "example2");

    /* Kernel execution */
    sclManageArgsLaunchKernel(hardware[DEVICE], kernel,
                              global_size, local_size,
                              "%R %a %N",
                              dataSize, (void *) vector, sizeof(float), (void *) &value, sizeof(float));

    /* Data is read back from the device automatically  ##### HERE ENDS THE SimpleOpenCL CODE ####*/
    /* We print some values to check the results */

    printf("\nExecution successful\n");
    printf("vector[0]=%f vector[10]=%f vector[200]=%f\n", vector[0], vector[10], vector[200]);
}

void generateRandomPicture(const int width, const int height, const int stride, const int value_range,struct Picture *picture){
    int num_pixels = (height+EDGE*2)*stride;
    picture->comp1 = (unsigned char*)malloc(sizeof(unsigned char)*num_pixels);
    picture->comp2 = (unsigned char*)malloc(sizeof(unsigned char)*num_pixels);
    picture->comp3 = (unsigned char*)malloc(sizeof(unsigned char)*num_pixels);

    srand(time(NULL));
    for(int i = 0;i<num_pixels;i++){
        picture->comp1[i] = (unsigned char)(rand()%value_range);
        picture->comp2[i] = (unsigned char)(rand()%value_range);
        picture->comp3[i] = (unsigned char)(rand()%value_range);
    }

    picture->comp1+=(stride*EDGE) + EDGE;
    picture->comp2+=(stride*EDGE) + EDGE;
    picture->comp3+=(stride*EDGE) + EDGE;
    picture->height = height;
    picture->width=width;
    picture->stride = stride;
}
